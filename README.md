# TInvest CLI

TInvest CLI is an CLI for getting info about instruments and making orders.

## Installation

Prerequisites:
- Java 11+

Perform script in the directory of this project:

Build and make an alias:
```shell
./gradlew installDist
alias tinvest=$PWD/build/apps/tinvest-cli-app/install/tinvest-cli-app/bin/tinvest-cli-app
```

Check that CLI was installed successfully:
```shell
tinvest --help
```

Define Tinkoff Invest API token:
```shell
unset HISTFILE
export TINVEST_TOKEN=<your token>
```

Try get data from Tinkoff Invest API:
```shell
tinvest asset ls
``` 

## Examples

1. Get accounts:

```shell
tinvest account ls
```

2. Submit buy limit order:

```shell
tinvest order create limit buy \
    --account-id=<account id> --ticker=VKCO \
    --quantity=1 --price=1000
```

3. Submit sell limit order:

```shell
tinvest order create limit sell \
    --account-id=<account id> --ticker=VKCO \
    --quantity=1 --price=1000
```
