import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTargetWithHostTests

plugins {
    id(BuildPlugins.TINVESTCLI_COMPONENT)
    application
}

kotlin {

    fun KotlinNativeTargetWithHostTests.configureBinaries() {
        binaries {
            executable()
        }
    }

    js(IR) {
        binaries.executable()
    }
    linuxX64 {
        configureBinaries()
    }
    mingwX64 {
        configureBinaries()
    }
    macosX64 {
        configureBinaries()
    }
    macosArm64 {
        configureBinaries()
    }

    sourceSets {
        commonMain {
            dependencies {
                implementation(projects.externals.tinkoffInvest.client)
                implementation(libs.clikt)
                implementation(libs.kotlinx.coroutines.core)
            }
        }
    }
}

application {
    mainClass.set("MainKt")
}
