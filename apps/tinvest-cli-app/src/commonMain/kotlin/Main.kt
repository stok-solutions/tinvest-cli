import com.github.ajalt.clikt.core.subcommands
import dev.stok.tinvest.cli.TinvestCli
import dev.stok.tinvest.cli.account.AccountCommand
import dev.stok.tinvest.cli.account.AccountInfoCommand
import dev.stok.tinvest.cli.account.AccountListCommand
import dev.stok.tinvest.cli.asset.AssetCommand
import dev.stok.tinvest.cli.asset.AssetListCommand
import dev.stok.tinvest.cli.instrument.InstrumentCommand
import dev.stok.tinvest.cli.instrument.InstrumentCountCommand
import dev.stok.tinvest.cli.instrument.InstrumentListCommand
import dev.stok.tinvest.cli.order.*
import dev.stok.tinvest.cli.util.UtilCommand
import dev.stok.tinvest.cli.util.UtilMapIdCommand

fun main(args: Array<String>) {
    TinvestCli() // tinvest
        .subcommands(
            AccountCommand() // .account
                .subcommands(
                    AccountListCommand(), // ..list
                    AccountInfoCommand(), // ..info
                ),
            AssetCommand() // .asset
                .subcommands(
                    AssetListCommand(), // ..list
                ),
            OrderCommand() // .order
                .subcommands(
                    OrderCreateCommand() // ..create
                        .subcommands(
                            OrderCreateMarketCommand(), // ...market
                            OrderCreateLimitCommand(), // ...limit
                        ),
                    OrderCancelCommand(), // ..cancel
                ),
            InstrumentCommand() // .instrument
                .subcommands(
                    InstrumentCountCommand(), // ..count
                    InstrumentListCommand(), // ..list
                    // InstrumentInfoCommand(), // ..info
                ),
            UtilCommand() // .util
                .subcommands(
                    UtilMapIdCommand(), // ..mapid
                )
//            PortfolioCommand(),
//            SandboxCommand(),
//            StocksCommand(),
        )
        .main(args)
}
