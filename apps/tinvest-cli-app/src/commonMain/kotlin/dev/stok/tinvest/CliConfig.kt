package dev.stok.tinvest

import dev.stok.external.tinvest.client.TinkoffInvestClient

data class CliConfig(
    val apiAddress: String,
    val token: String,
    val client: TinkoffInvestClient,
)
