package dev.stok.tinvest.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.help
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import dev.stok.external.tinvest.client.TinkoffInvestClient
import dev.stok.tinvest.CliConfig

class TinvestCli : CliktCommand(
    name = "tinvest",
    help = "Tinkoff Invest CLI",
    epilog = "Made with ❤️ by Stok Team",
    printHelpOnEmptyArgs = true,
) {

    override fun aliases(): Map<String, List<String>> = mapOf(
        "a" to listOf("account"),
        "ass" to listOf("asset"),
        "o" to listOf("order"),
        "i" to listOf("instrument"),
    )

    private val apiAddress: String by option(envvar = "TINVEST_API_ADDRESS")
        .default("invest-public-api.tinkoff.ru:443")
        .help(
            """
                Tinkoff Invest API address.
                Can be provided with environment variable 'TINVEST_API_ADDRESS'.
                Default: invest-public-api.tinkoff.ru:443
            """.trimIndent(),
        )

    private val token: String by option(envvar = "TINVEST_TOKEN").required()
        .help(
            """
                Tinkoff Invest API token.
                Can be provided with environment variable 'TINVEST_TOKEN'.
            """.trimIndent(),
        )

    override fun run() {
        currentContext.obj = CliConfig(
            apiAddress = apiAddress,
            token = token,
            client = TinkoffInvestClient(token),
        )
    }
}
