package dev.stok.tinvest.cli.account

import com.github.ajalt.clikt.core.CliktCommand

class AccountCommand : CliktCommand(name = "account", help = "List, get detailed info about account(s)") {

    override fun aliases(): Map<String, List<String>> = mapOf(
        "ls" to listOf("list"),
        "i" to listOf("info"),
    )

    override fun run() {
        // Nothing to do
    }
}
