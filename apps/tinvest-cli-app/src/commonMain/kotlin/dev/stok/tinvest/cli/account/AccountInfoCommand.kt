package dev.stok.tinvest.cli.account

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import dev.stok.tinvest.CliConfig
import dev.stok.tinvest.util.runBlocking

class AccountInfoCommand : CliktCommand(name = "info", help = "Get detailed info about specified account") {

    private val config: CliConfig by requireObject()

    override fun run() = runBlocking {
        TODO("Not yet implemented")
    }
}
