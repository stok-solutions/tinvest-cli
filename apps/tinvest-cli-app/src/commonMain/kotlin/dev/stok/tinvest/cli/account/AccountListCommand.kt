package dev.stok.tinvest.cli.account

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import dev.stok.tinvest.CliConfig
import dev.stok.tinvest.util.runBlocking

class AccountListCommand : CliktCommand(name = "list", help = "List accounts") {

    private val config: CliConfig by requireObject()

    override fun run() = runBlocking {
        config.client.accounts.getAccounts()
            .forEach { echo(it.toString()) }
    }
}
