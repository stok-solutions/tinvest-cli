package dev.stok.tinvest.cli.asset

import com.github.ajalt.clikt.core.CliktCommand

class AssetCommand : CliktCommand(name = "asset", help = "List, get detailed info about asset(s)") {

    override fun aliases(): Map<String, List<String>> = mapOf(
        "ls" to listOf("list"),
        "i" to listOf("info"),
    )

    override fun run() {
        // Nothing to do
    }
}
