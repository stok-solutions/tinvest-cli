package dev.stok.tinvest.cli.asset

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.multiple
import com.github.ajalt.clikt.parameters.options.multiple
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.enum
import dev.stok.external.tinvest.client.enum.InstrumentType
import dev.stok.external.tinvest.client.enum.TradeClassCode
import dev.stok.external.tinvest.client.model.Asset
import dev.stok.tinvest.CliConfig
import dev.stok.tinvest.enum.CliInstrumentType
import dev.stok.tinvest.util.runBlocking
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlin.jvm.JvmInline

class AssetListCommand : CliktCommand(name = "list", help = "List assets") {

    private val config: CliConfig by requireObject()

    private val tokens: List<String> by argument(help = "List of optional tokens to search in result").multiple()

    private val types by option(
        names = arrayOf("--type"),
        help = """
            Instrument type.
            Usage: `--type stock --type bond`
        """.trimIndent(),
    )
        .enum<CliInstrumentType>()
        .multiple()

    override fun run() =
        fetchList().asSequence()
            .filter { ass ->
                val assetTokens = tokensOf(ass)
                tokens.isEmpty() || tokens.all { it in assetTokens }
            }
            .forEach { echo(it.repr()) }

    private fun fetchList(): List<Asset> = runBlocking block@{
        val instrumentsClient = config.client.instruments

        if (types.isEmpty()) {
            return@block instrumentsClient.getAllAssets()
        }

        return@block types.map {
            async {
                when (it) {
                    CliInstrumentType.currency -> instrumentsClient.getAssetsByInstrumentType(type = InstrumentType.CURRENCY)
                    CliInstrumentType.stock -> instrumentsClient.getAssetsByInstrumentType(type = InstrumentType.SHARE)
                    CliInstrumentType.etf -> instrumentsClient.getAssetsByInstrumentType(type = InstrumentType.ETF)
                    CliInstrumentType.bond -> instrumentsClient.getAssetsByInstrumentType(type = InstrumentType.BOND)
                    CliInstrumentType.future -> instrumentsClient.getAssetsByInstrumentType(type = InstrumentType.FUTURE)
                    CliInstrumentType.structural_note -> instrumentsClient.getAssetsByInstrumentType(type = InstrumentType.STRUCTURAL_NOTE)
                    CliInstrumentType.option -> instrumentsClient.getAssetsByInstrumentType(type = InstrumentType.OPTION)
                    CliInstrumentType.commodity -> error("Not implemented")
                }
            }
        }
            .awaitAll()
            .flatten()
    }

    private companion object {

        private fun Asset.repr(): String = toString()

        private fun tokensOf(asset: Asset): Tokens = asset.run {
            Tokens(
                buildList list@{
                    this@list += uid.toString()
                    this@list += type.toString()
                    this@list += name
                    this@list += firstFigiOrNull()
                    this@list += firstTickerOrNull()
                    addAll(classCodes().map(TradeClassCode::toString))
                    this@list += instruments.firstNotNullOfOrNull(Asset.AssetInstrument::type)
                    this@list += instruments.firstNotNullOfOrNull(Asset.AssetInstrument::kind)?.toString()
                    this@list += firstPositionUidOrNull()?.toString()
                }.filterNotNull()
            )
        }
    }

    @JvmInline
    value class Tokens(private val tokens: List<String>) {

        operator fun contains(value: String): Boolean {
            val uppercaseValue = value.uppercase()
            val lowercaseValue = value.lowercase()

            val uppercaseTokens = tokens.map(String::uppercase)
            val lowercaseTokens = tokens.map(String::lowercase)

            return uppercaseTokens.any { uppercaseValue in it } ||
                lowercaseTokens.any { lowercaseValue in it } ||
                tokens.any { value in it }
        }
    }
}
