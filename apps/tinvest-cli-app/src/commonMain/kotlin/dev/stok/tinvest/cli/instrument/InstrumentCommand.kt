package dev.stok.tinvest.cli.instrument

import com.github.ajalt.clikt.core.CliktCommand

class InstrumentCommand : CliktCommand(name = "instrument", help = "List, get detailed info about instrument(s)") {

    override fun aliases(): Map<String, List<String>> = mapOf(
        "c" to listOf("count"),
        "ls" to listOf("list"),
        "i" to listOf("info"),
    )

    override fun run() {
        // Nothing to do
    }
}
