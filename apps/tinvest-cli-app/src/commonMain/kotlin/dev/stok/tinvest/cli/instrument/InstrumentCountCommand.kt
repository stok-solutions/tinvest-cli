package dev.stok.tinvest.cli.instrument

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import dev.stok.external.tinvest.client.model.Asset
import dev.stok.tinvest.CliConfig
import dev.stok.tinvest.util.runBlocking

class InstrumentCountCommand : CliktCommand(name = "count", help = "Count instruments available in API") {

    private val config: CliConfig by requireObject()

    override fun run() =
        echo(fetchList().size)

    private fun fetchList(): List<Asset> = runBlocking {
        val instrumentsClient = config.client.instruments

        instrumentsClient.getAllAssets().asSequence()
            .filter { it.firstFigiOrNull() != null }
            .toList()
    }
}
