package dev.stok.tinvest.cli.instrument

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.int
import com.github.ajalt.clikt.parameters.types.restrictTo
import dev.stok.external.tinvest.client.model.Instrument
import dev.stok.tinvest.CliConfig
import dev.stok.tinvest.util.runBlocking
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll

class InstrumentListCommand : CliktCommand(name = "list", help = "List instruments") {

    private val config: CliConfig by requireObject()

    private val from by option(names = arrayOf("--from"), help = "Offset")
        .int()
        .restrictTo(min = 1)
        .default(1)

    private val count by option(names = arrayOf("--count"), help = "Count of instruments")
        .int()
        .restrictTo(min = 1, max = 100)
        .default(100)

    override fun run() =
        fetchList()
            .forEach { echo(it.toString()) }

    private fun fetchList(): List<Instrument> = runBlocking {
        val instrumentsClient = config.client.instruments

        val pivotalInstruments = instrumentsClient.getAllAssets().asSequence()
            .drop((from - 1)).take(count)
            .filter { it.firstFigiOrNull() != null }
            .toList()

        pivotalInstruments
            .map {
                GlobalScope.async {
                    instrumentsClient.getInstrumentByFigi(it.firstFigiOrNull()!!)
                }
            }
            .awaitAll()
    }
}
