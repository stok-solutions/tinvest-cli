package dev.stok.tinvest.cli.order

import com.benasher44.uuid.Uuid
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.convert
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import dev.stok.tinvest.CliConfig
import dev.stok.tinvest.util.runBlocking
import dev.stok.tinvest.util.toUuid

class OrderCancelCommand : CliktCommand(name = "cancel", help = "Cancel order by id") {

    private val config: CliConfig by requireObject()

    private val orderId: Uuid by argument(help = "Order id").convert { it.toUuid() }

    private val accountId: String by option(help = "Account ID").required()

    override fun run() = runBlocking {
        config.client.orders.cancelOrder(accountId = accountId, orderId = orderId)
        echo("Order [$orderId] was closed")
    }
}
