package dev.stok.tinvest.cli.order

import com.github.ajalt.clikt.core.CliktCommand

class OrderCommand : CliktCommand(name = "order", help = "List, create or cancel order(s)") {

    override fun aliases(): Map<String, List<String>> = mapOf(
        "c" to listOf("create"),
        "ls" to listOf("list"),
        "x" to listOf("cancel"),
    )

    override fun run() {
        // Nothing to do
    }
}
