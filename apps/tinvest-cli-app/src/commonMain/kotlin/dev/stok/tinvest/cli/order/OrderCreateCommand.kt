package dev.stok.tinvest.cli.order

import com.github.ajalt.clikt.core.CliktCommand

class OrderCreateCommand : CliktCommand(name = "create", help = "Create market/limit order") {

    override fun aliases(): Map<String, List<String>> = mapOf(
        "mb" to listOf("market", "buy"),
        "ms" to listOf("market", "sell"),
        "lb" to listOf("limit", "buy"),
        "ls" to listOf("limit", "sell"),
    )

    override fun run() {
        // Nothing to do
    }
}
