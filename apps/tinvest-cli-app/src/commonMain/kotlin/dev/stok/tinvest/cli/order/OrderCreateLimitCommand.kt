package dev.stok.tinvest.cli.order

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.groups.required
import com.github.ajalt.clikt.parameters.groups.single
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.types.choice
import com.github.ajalt.clikt.parameters.types.double
import com.github.ajalt.clikt.parameters.types.long
import dev.stok.tinvest.CliConfig
import dev.stok.tinvest.enum.InstrumentIdType
import dev.stok.tinvest.model.InstrumentId
import dev.stok.tinvest.service.InstrumentService
import dev.stok.tinvest.util.instrumentOption
import dev.stok.tinvest.util.runBlocking

class OrderCreateLimitCommand : CliktCommand(name = "limit", help = "Create limit order") {

    private val config: CliConfig by requireObject()

    private val action: String by argument(help = "Action").choice(ACTION_BUY, ACTION_SELL)

    private val instrumentId: InstrumentId by instrumentOption().single().required()

    private val price by option(help = "Price").double().required()

    private val quantity by option(help = "Quantity").long().required()

    private val accountId by option(help = "Account ID").required()

    override fun run() = runBlocking {
        val uid = InstrumentService(config.client).convertInstrumentId(
            instrumentId = instrumentId,
            targetType = InstrumentIdType.UID,
        )

        prompt(
            text = "Are you sure you want to create a $action limit order" +
                " for instrument ${instrumentId.value} (uid=${uid.value})?",
            default = "y",
            choices = listOf("y", "n"),
        ).takeIf { it == "y" } ?: return@runBlocking

        val orderId = when (action) {
            ACTION_BUY -> config.client.orders.limitBuy(
                instrumentId = uid.value,
                price = price,
                quantity = quantity,
                accountId = accountId,
            )

            ACTION_SELL -> config.client.orders.limitSell(
                instrumentId = uid.value,
                price = price,
                quantity = quantity,
                accountId = accountId,
            )

            else -> error("Unknown action: $action")
        }

        echo("Order ID: $orderId")
    }

    private companion object {
        const val ACTION_BUY: String = "buy"
        const val ACTION_SELL: String = "sell"
    }
}
