package dev.stok.tinvest.cli.util

import com.github.ajalt.clikt.core.CliktCommand

class UtilCommand : CliktCommand(name = "util", help = "Utility commands") {

    override fun run() {
        // Nothing to do
    }
}
