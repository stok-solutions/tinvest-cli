package dev.stok.tinvest.cli.util

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.multiple
import com.github.ajalt.clikt.parameters.types.enum
import dev.stok.tinvest.CliConfig
import dev.stok.tinvest.enum.InstrumentIdType
import dev.stok.tinvest.model.InstrumentId
import dev.stok.tinvest.service.InstrumentService
import dev.stok.tinvest.util.runBlocking
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.supervisorScope

class UtilMapIdCommand : CliktCommand(name = "mapid", help = "Map identifier to another type") {

    private val config: CliConfig by requireObject()

    private val fromType by argument(name = "from-type", help = "Type of given identifier")
        .enum<InstrumentIdType> { it.name.lowercase() }

    private val targetType by argument(name = "target-type", help = "Target identifier type")
        .enum<InstrumentIdType> { it.name.lowercase() }

    private val ids by argument(name = "ids", help = "Identifiers to be mapped").multiple(required = true)

    override fun run() = runBlocking {
        val instrumentService = InstrumentService(config.client)

        val targetIds = supervisorScope {
            ids.map { id ->
                GlobalScope.async {
                    runCatching {
                        instrumentService.convertInstrumentId(
                            instrumentId = when (fromType) {
                                InstrumentIdType.FIGI -> InstrumentId.Figi(id)
                                InstrumentIdType.TICKER -> InstrumentId.Ticker(id)
                                InstrumentIdType.UID -> InstrumentId.Uid(id)
                            },
                            targetType = targetType,
                        )
                    }.getOrNull()
                }
            }.awaitAll()
        }

        for (targetId in targetIds) {
            echo(targetId?.value)
        }
    }
}
