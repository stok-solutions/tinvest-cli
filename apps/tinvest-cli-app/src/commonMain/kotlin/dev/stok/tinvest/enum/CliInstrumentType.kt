package dev.stok.tinvest.enum

@Suppress("ktlint:standard:enum-entry-name-case")
enum class CliInstrumentType {
    currency,
    stock,
    etf,
    bond,
    future,
    structural_note,
    option,
    commodity,
}
