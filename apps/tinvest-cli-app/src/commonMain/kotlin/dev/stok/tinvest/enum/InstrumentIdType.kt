package dev.stok.tinvest.enum

enum class InstrumentIdType {
    TICKER,
    FIGI,
    UID,
}
