package dev.stok.tinvest.model

import dev.stok.tinvest.enum.InstrumentIdType

sealed class InstrumentId(
    val type: InstrumentIdType,
    val value: String,
) {

    class Ticker(value: String) : InstrumentId(type = InstrumentIdType.TICKER, value = value)

    class Figi(value: String) : InstrumentId(type = InstrumentIdType.FIGI, value = value)

    class Uid(value: String) : InstrumentId(type = InstrumentIdType.UID, value = value)

    override fun hashCode(): Int {
        var result = type.hashCode()
        result = 31 * result + value.hashCode()
        return result
    }

    override fun toString(): String = "InstrumentId(type=$type, value='$value')"
}
