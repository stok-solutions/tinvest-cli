package dev.stok.tinvest.service

import dev.stok.external.tinvest.client.TinkoffInvestClient
import dev.stok.external.tinvest.client.enum.TradeClassCode
import dev.stok.external.tinvest.client.model.Instrument
import dev.stok.tinvest.enum.InstrumentIdType
import dev.stok.tinvest.model.InstrumentId
import dev.stok.tinvest.model.InstrumentId.Figi
import dev.stok.tinvest.model.InstrumentId.Ticker
import dev.stok.tinvest.model.InstrumentId.Uid
import dev.stok.tinvest.util.toUuid
import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlin.coroutines.cancellation.CancellationException

class InstrumentService(
    private val client: TinkoffInvestClient,
) {

//    private val logger: KLogger = KotlinLogging.logger(this::class.qualifiedName!!)

    suspend fun convertInstrumentId(instrumentId: InstrumentId, targetType: InstrumentIdType): InstrumentId {
        if (instrumentId.type == targetType) {
            return instrumentId
        }

        val domainInstrument = getDomainInstrumentInfo(instrumentId)
        return when (targetType) {
            InstrumentIdType.FIGI -> Figi(domainInstrument.figi)
            InstrumentIdType.TICKER -> Ticker(domainInstrument.ticker)
            InstrumentIdType.UID -> Uid(domainInstrument.uid.toString())
            else -> error("Instrument type [$targetType] is not supported")
        }
    }

    private suspend fun getDomainInstrumentInfo(instrumentId: InstrumentId): Instrument =
        try {
            when (instrumentId) {
                is Ticker -> TradeClassCode.values()
                    .map {
                        runCatching {
                            client.instruments.getInstrumentByTicker(instrumentId.value, it)
                        }
                    }
                    .firstNotNullOf(Result<Instrument>::getOrNull)

                is Figi -> client.instruments.getInstrumentByFigi(instrumentId.value)

                is Uid -> client.instruments.getInstrumentByUid(instrumentId.value.toUuid())
            }
        } catch (ex: Throwable) {
            if (ex is CancellationException) throw ex
            throw ex
//            throw logger.throwing(RuntimeException("Could not get instrument info for [$instrumentId]", ex))
        }
}
