package dev.stok.tinvest.util

import com.github.ajalt.clikt.core.ParameterHolder
import com.github.ajalt.clikt.parameters.groups.MutuallyExclusiveOptions
import com.github.ajalt.clikt.parameters.groups.mutuallyExclusiveOptions
import com.github.ajalt.clikt.parameters.options.convert
import com.github.ajalt.clikt.parameters.options.option
import dev.stok.tinvest.model.InstrumentId

fun ParameterHolder.instrumentOption(): MutuallyExclusiveOptions<InstrumentId, InstrumentId?> =
    mutuallyExclusiveOptions(
        option("--ticker", help = "Ticker").convert { InstrumentId.Ticker(it) },
        option("--figi", help = "FIGI").convert { InstrumentId.Figi(it) },
        option("--uid", help = "FIGI").convert { InstrumentId.Uid(it) },
    )
