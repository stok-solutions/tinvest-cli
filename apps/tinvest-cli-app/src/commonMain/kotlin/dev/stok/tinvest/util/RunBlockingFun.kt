package dev.stok.tinvest.util

import kotlinx.coroutines.CoroutineScope

inline fun <T> runBlocking(noinline block: suspend CoroutineScope.() -> T): T =
    kotlinx.coroutines.runBlocking<T>(block = block)
