package dev.stok.tinvest.util

import com.benasher44.uuid.Uuid

fun String.toUuid(): Uuid = Uuid.fromString(this)
