plugins {
    `kotlin-dsl`
}

buildDir = run {
    val globalBuildDir: File = projectDir.parentFile.parentFile.resolve("build")
    globalBuildDir.resolve("buildSrc").resolve("buildSrc")
}

dependencies {
    implementation(libs.gradlePlugins.kotlin)
}
