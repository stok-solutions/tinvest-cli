object BuildPlugins {

    const val SPOTLESS = "com.diffplug.spotless"
    const val DOKKA = "org.jetbrains.dokka"
    const val KOTLINX_KOVER = "org.jetbrains.kotlinx.kover"

    const val KOTLIN_MULTIPLATFORM = "org.jetbrains.kotlin.multiplatform"
    const val KOTEST_MULTIPLATFORM = "io.kotest.multiplatform"

    const val TINVESTCLI_BASE = "tinvestcli.base"
    const val TINVESTCLI_COMPONENT = "tinvestcli.component"
}
