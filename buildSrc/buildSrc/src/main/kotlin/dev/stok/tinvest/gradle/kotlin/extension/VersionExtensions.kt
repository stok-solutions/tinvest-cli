package dev.stok.tinvest.gradle.kotlin.extension

import org.gradle.api.JavaVersion
import org.gradle.api.Project
import org.gradle.api.provider.Provider

/*internal*/ val Project.projectVersion: Provider<String>
    get() = versionCatalog.map { libs -> libs.versions.tinvestCli.requiredVersion }

/*internal*/ val Project.projectVersionCode: Provider<String>
    get() = projectVersion.map { it.split("-").first() }

/*internal*/ val Project.javaVersion: Provider<JavaVersion>
    get() = versionCatalog.map { libs ->
        libs.versions.java
            .requiredVersion
            .let(JavaVersion::toVersion)
    }

/*internal*/ val Project.kotlinVersion: Provider<KotlinVersion>
    get() = versionCatalog.map { libs ->
        libs.versions.kotlin.requiredVersion
            .split('.').map(String::toInt)
            .let { (major, minor, patch) -> KotlinVersion(major, minor, patch) }
    }
