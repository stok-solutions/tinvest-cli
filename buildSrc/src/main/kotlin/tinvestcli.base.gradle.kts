import dev.stok.tinvest.gradle.kotlin.extension.projectVersion

if (project == rootProject) {
    plugins.apply(BuildPlugins.DOKKA)
}

version = projectVersion.get()

buildDir = run {
    val globalBuildDir: File = rootProject.projectDir.resolve("build")
    when (project) {
        rootProject -> globalBuildDir.resolve(project.name)
        else -> {
            val relativeProjectPath = projectDir.relativeTo(rootProject.projectDir)
            globalBuildDir.resolve(relativeProjectPath)
        }
    }
}
