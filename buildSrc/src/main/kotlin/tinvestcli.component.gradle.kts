import com.diffplug.spotless.LineEnding
import dev.stok.tinvest.gradle.internal.ktlintEditorConfig
import dev.stok.tinvest.gradle.kotlin.extension.javaVersion
import dev.stok.tinvest.gradle.kotlin.extension.kotlinVersion
import dev.stok.tinvest.gradle.kotlin.extension.versionCatalog
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.nio.charset.Charset

plugins {
    id("tinvestcli.base")
    kotlin("multiplatform")
    id("io.kotest.multiplatform")
    id("com.diffplug.spotless")
}

kotlin {
    jvmToolchain(javaVersion.get().getMajorVersion().toInt())
    jvm {
        withJava()
        testRuns["test"].executionTask.configure {
            useJUnitPlatform()
        }
    }

    js(IR) {
        useCommonJs()

        nodejs()
        browser {
            webpackTask {
                output.libraryTarget = "commonjs2"
            }
        }
    }

    linuxX64()
    mingwX64()
    macosX64()
    macosArm64()

    sourceSets {
        val libs = versionCatalog.get()

        all {
            languageSettings {
                listOf(
                    "kotlin.RequiresOptIn",
                    "kotlin.time.ExperimentalTime",
                    "kotlinx.coroutines.DelicateCoroutinesApi",
                    "kotlinx.serialization.ExperimentalSerializationApi",
                    "kotlin.io.encoding.ExperimentalEncodingApi",
                ).forEach(::optIn)
            }
        }

        val commonMain by getting {
            dependencies {
                api(libs.kotlinx.coroutinesCore)
                api(libs.kotlinx.datetime)
                api(libs.kotlinLogging)
            }
        }

        val commonTest by getting {
            dependencies {
                api(libs.kotlin.test)
                api(libs.kotlin.test.annotationsCommon)
                api(libs.kotest.assertionsCore)
                api(libs.kotest.property)
                api(libs.kotest.frameworkDatatest)
                api(libs.kotest.frameworkApi)
                api(libs.kotest.frameworkEngine)
            }
        }

        val jvmTest by getting {
            dependencies {
                implementation(libs.kotest.runnerJunit5)
                implementation(libs.kotlin.test.junit5)
            }
        }

        val nativeMain by creating {
            dependsOn(commonMain)
        }
        val linuxX64Main by getting {
            dependsOn(nativeMain)
        }
        val mingwX64Main by getting {
            dependsOn(nativeMain)
        }
        val nativeDarwinMain by creating {
            dependsOn(nativeMain)
        }
        val macosX64Main by getting {
            dependsOn(nativeDarwinMain)
        }
        val macosArm64Main by getting {
            dependsOn(nativeDarwinMain)
        }

        val nativeTest by creating {
            dependsOn(commonTest)
        }
        val linuxX64Test by getting {
            dependsOn(nativeTest)
        }
        val mingwX64Test by getting {
            dependsOn(nativeTest)
        }
        val nativeDarwinTest by creating {
            dependsOn(nativeTest)
        }
        val macosX64Test by getting {
            dependsOn(nativeDarwinTest)
        }
        val macosArm64Test by getting {
            dependsOn(nativeDarwinTest)
        }
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = javaVersion.get().majorVersion

        val kotlinRelease = kotlinVersion.get().run { "$major.$minor" }
        apiVersion = kotlinRelease
        languageVersion = kotlinRelease
    }
}

spotless {
    kotlin {
        target("**/*.kt")
        ktlint(versionCatalog.get().versions.gradlePlugins.ktlint.requiredVersion)
            .editorConfigOverride(ktlintEditorConfig)
        lineEndings = LineEnding.UNIX
        encoding = Charset.forName("UTF-8")
        trimTrailingWhitespace()
        endWithNewline()
        indentWithSpaces(4)
    }
}
