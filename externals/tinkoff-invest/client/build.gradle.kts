plugins {
    id(BuildPlugins.TINVESTCLI_COMPONENT)
}

kotlin {

    sourceSets {
        commonMain {
            dependencies {
                api(libs.benasherUuid)
            }
        }
        jvmMain {
            dependencies {
                api(projects.externals.tinkoffInvest.stub)
                runtimeOnly("io.grpc:grpc-netty:${libs.versions.grpc.get()}")
            }
        }
    }
}
