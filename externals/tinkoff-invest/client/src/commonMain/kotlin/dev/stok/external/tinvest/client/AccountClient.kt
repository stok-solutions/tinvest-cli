package dev.stok.external.tinvest.client

import dev.stok.external.tinvest.client.model.Account

interface AccountClient {

    suspend fun getAccounts(): List<Account>
}
