package dev.stok.external.tinvest.client

import com.benasher44.uuid.Uuid
import dev.stok.external.tinvest.client.enum.InstrumentType
import dev.stok.external.tinvest.client.enum.TradeClassCode
import dev.stok.external.tinvest.client.model.Asset
import dev.stok.external.tinvest.client.model.Instrument

interface InstrumentClient {

    suspend fun getAllAssets(): List<Asset>

    suspend fun getAssetsByInstrumentType(type: InstrumentType): List<Asset>

    suspend fun getInstrumentByUid(uid: Uuid): Instrument

    suspend fun getInstrumentByPositionUid(positionUid: Uuid): Instrument

    suspend fun getInstrumentByFigi(figi: String): Instrument

    suspend fun getInstrumentByTicker(ticker: String, classCode: TradeClassCode): Instrument
}
