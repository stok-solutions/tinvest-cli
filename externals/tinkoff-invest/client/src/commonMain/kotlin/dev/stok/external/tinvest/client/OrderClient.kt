package dev.stok.external.tinvest.client

import com.benasher44.uuid.Uuid

interface OrdersClient {

    suspend fun marketBuy(instrumentId: String, quantity: Long, accountId: String): Uuid

    suspend fun marketSell(instrumentId: String, quantity: Long, accountId: String): Uuid

    suspend fun limitBuy(instrumentId: String, price: Double, quantity: Long, accountId: String): Uuid

    suspend fun limitSell(instrumentId: String, price: Double, quantity: Long, accountId: String): Uuid

    suspend fun cancelOrder(orderId: Uuid, accountId: String)
}
