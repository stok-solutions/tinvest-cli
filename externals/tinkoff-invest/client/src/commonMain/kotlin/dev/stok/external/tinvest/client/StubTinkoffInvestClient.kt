package dev.stok.external.tinvest.client

import dev.stok.external.tinvest.client.enum.InstrumentType
import dev.stok.external.tinvest.client.model.Asset

class StubTinkoffInvestClient(token: String) {

    val accounts: AccountClient get() = TODO("Not yet implemented")

    val instruments: InstrumentClient get() = TODO("Not yet implemented")

    suspend fun getAssetsByInstrumentType(type: InstrumentType): List<Asset> {
        TODO("Not yet implemented")
    }
}
