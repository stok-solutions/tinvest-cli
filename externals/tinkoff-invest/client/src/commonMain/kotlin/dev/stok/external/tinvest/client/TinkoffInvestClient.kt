package dev.stok.external.tinvest.client

expect class TinkoffInvestClient(token: String) {

    val accounts: AccountClient

    val instruments: InstrumentClient

    val orders: OrdersClient
}
