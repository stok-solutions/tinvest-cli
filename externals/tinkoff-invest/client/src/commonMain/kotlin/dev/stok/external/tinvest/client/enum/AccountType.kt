package dev.stok.external.tinvest.client.enum

enum class AccountType {
    PLAIN,
    IIS,
    INVEST_BOX,
}
