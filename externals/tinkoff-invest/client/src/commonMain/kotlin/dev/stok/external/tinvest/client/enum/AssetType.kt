package dev.stok.external.tinvest.client.enum

enum class AssetType {

    UNSPECIFIED,
    CURRENCY,
    COMMODITY,
    INDEX,
    SECURITY,
    UNRECOGNIZED,
}
