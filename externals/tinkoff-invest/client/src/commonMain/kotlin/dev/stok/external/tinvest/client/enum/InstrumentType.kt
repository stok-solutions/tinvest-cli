package dev.stok.external.tinvest.client.enum

enum class InstrumentType {
    BOND,
    SHARE,
    CURRENCY,
    ETF,
    FUTURE,
    STRUCTURAL_NOTE,
    OPTION,
    CLEARING_CERTIFICATE,
}
