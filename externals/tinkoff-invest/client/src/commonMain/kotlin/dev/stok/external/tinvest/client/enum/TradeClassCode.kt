package dev.stok.external.tinvest.client.enum

/**
 * classCode=BE0000340498,
 * classCode=CETS,
 * classCode=DE0001135424,
 * classCode=DOFSQ,
 * classCode=EQOB,
 * classCode=FR0013140035,
 * classCode=IT0005188120,
 * classCode=NL0010721999,
 * classCode=PSAU,
 * classCode=PTOTEYOE0007,
 * classCode=SBTBSLT,
 * classCode=SPBBND,
 * classCode=SPBHKEX,
 * classCode=SPBKZ,
 * classCode=SPBRU,
 * classCode=SPBXM,
 * classCode=SPHKTF_CNY,
 * classCode=SPHKTF_HKD,
 * classCode=TQBR,
 * classCode=TQCB,
 * classCode=TQIF,
 * classCode=TQIR,
 * classCode=TQIY,
 * classCode=TQOB,
 * classCode=TQOD,
 * classCode=TQOE,
 * classCode=TQOY,
 * classCode=TQPI,
 * classCode=TQRD,
 * classCode=TQTD,
 * classCode=TQTE,
 * classCode=TQTF,
 * classCode=US06051GFW42,
 * classCode=US191216CP30,
 * classCode=US191216CQ13,
 * classCode=US68389XBL82,
 * classCode=US904764AZ08,
 * classCode=US912810SL35,
 * classCode=XS2264155305,
 * classCode=atonbonds,
 * classCode=null,
 */

/**
 * [GlobalExchanges. RUSSIA: MOEX Starts Trading in SBP Exchange Shares](https://globalexchanges.com/latest-news/russia-moex-starts-trading-in-sbp-exchange-shares/127704/)
 *
 * [MOEX. Preliminary parameters for the start of trading in SPB Exchange ordinary shares](https://www.moex.com/n37801)
 */
enum class TradeClassCode(val code: String) {

    // MOEX
    TQBE("TQBE"),

    CENTRAL_ORDER_BOOK("TQBR"),

    TQCB("TQCB"),

    TQIF("TQIF"),

    TQIR("TQIR"),

    TQIY("TQIY"),

    TQOB("TQOB"),

    TQOD("TQOD"),

    TQOE("TQOE"),

    TQOY("TQOY"),

    TQPI("TQPI"),

    TQRD("TQRD"),

    TQTD("TQTD"),

    TQTE("TQTE"),

    TQTF("TQTF"),

    TQUD("TQUD"),

    TQED("TQED"),

    TQIU("TQIU"),

    TQIE("TQIE"),

    TQFD("TQFD"),

    TQFE("TQFE"),

    TQPD("TQPD"),

    TQPE("TQPE"),

    CETS("CETS"),

    // SPB
    SPOB("SPOB"),

    SBTBSLT("SBTBSLT"),

    SPBBND("SPBBND"),

    SPBHKEX("SPBHKEX"),

    SPBKZ("SPBKZ"),

    SPBRU("SPBRU"),

    SPBXM("SPBXM"),

    SPHKTF_CNY("SPHKTF_CNY"),

    SPHKTF_HKD("SPHKTF_HKD"),

    // NASDAQ
    // XNGS("XNGS"),

    // NYSE
    // XNYS("XNYS"),
    // ARCX("ARCX"),

    // IEX
    // IEXG("IEXG"),

    // LSE
    // SETS("SETS"),
    // SEAQ("SEAQ"),
    // IOB("IOB"),

    NEGOTIATED_TRADES("SPEQ"),

    NEGOTIATED_TRADES_WITH_CCP("PTEQ"),

    DOFSQ("DOFSQ"),

    PSAU("PSAU"),

    PSBB("PSBB"),

    PSBB_EQ("PSBB_EQ"),

    PSSU("PSSU"),

    FQBR("FQBR"),

    FQDE("FQDE"),

    UNKNOWN("UNKNOWN"),
    ;

    companion object {

        fun fromCode(value: String): TradeClassCode =
            TradeClassCode.values()
                .singleOrNull { it.code == value }
                ?: TradeClassCode.UNKNOWN
    }
}
