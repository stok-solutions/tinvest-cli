package dev.stok.external.tinvest.client.model

import dev.stok.external.tinvest.client.enum.AccountType

data class Account(
    val id: String,
    val type: AccountType,
    val name: String,
)
