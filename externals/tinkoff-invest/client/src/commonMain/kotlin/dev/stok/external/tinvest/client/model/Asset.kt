package dev.stok.external.tinvest.client.model

import com.benasher44.uuid.Uuid
import dev.stok.external.tinvest.client.enum.AssetType
import dev.stok.external.tinvest.client.enum.InstrumentType
import dev.stok.external.tinvest.client.enum.TradeClassCode

data class Asset(
    val uid: Uuid,
    val type: AssetType,
    val name: String,

    val instruments: List<AssetInstrument>,
) {

    fun classCodes(): List<TradeClassCode> =
        instruments.asSequence()
            .map(AssetInstrument::classCode)
            .filterNotNull()
            .toList()

    fun firstUidOrNull(): Uuid? = instruments.firstNotNullOfOrNull(AssetInstrument::uid)

    fun firstTickerOrNull(): String? = instruments.firstNotNullOfOrNull(AssetInstrument::ticker)

    fun firstFigiOrNull(): String? = instruments.firstNotNullOfOrNull(AssetInstrument::figi)

    fun firstPositionUidOrNull(): Uuid? = instruments.firstNotNullOfOrNull(AssetInstrument::positionUid)

    data class AssetInstrument(
        val uid: Uuid,
        val type: String,
        val kind: InstrumentType,
        val figi: String,
        val ticker: String,
        val positionUid: Uuid,
        val classCode: TradeClassCode,
        val links: List<InstrumentLink>,
    )

    data class InstrumentLink(
        val type: String,
        val instrumentUid: Uuid,
    )
}
