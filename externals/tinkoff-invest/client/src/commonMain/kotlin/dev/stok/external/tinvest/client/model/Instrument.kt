package dev.stok.external.tinvest.client.model

import com.benasher44.uuid.Uuid
import dev.stok.external.tinvest.client.enum.InstrumentType

data class Instrument(
    val uid: Uuid,
    val type: String,
    val kind: InstrumentType,
    val name: String,

    val figi: String,
    val isin: String,
    val ticker: String,

    val classCode: String,
    val positionUid: Uuid,

    val exchange: String,
    val currency: String,

    val countryOfRisk: CountryOfRisk,
) {

    data class CountryOfRisk(
        val code: String,
        val name: String,
    )
}
