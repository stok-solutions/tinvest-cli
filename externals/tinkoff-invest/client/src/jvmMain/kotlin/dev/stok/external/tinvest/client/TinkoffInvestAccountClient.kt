package dev.stok.external.tinvest.client

import dev.stok.external.tinvest.client.enum.AccountType
import dev.stok.external.tinvest.client.model.Account
import io.grpc.ManagedChannel
import ru.tinkoff.piapi.contract.v1.GetAccountsRequest
import ru.tinkoff.piapi.contract.v1.UsersServiceGrpcKt.UsersServiceCoroutineStub

private typealias DomainAccount = ru.tinkoff.piapi.contract.v1.Account
private typealias DomainAccountType = ru.tinkoff.piapi.contract.v1.AccountType

class TinkoffInvestAccountClient(channel: ManagedChannel) : AccountClient {

    private val usersService: UsersServiceCoroutineStub = UsersServiceCoroutineStub(channel)

    override suspend fun getAccounts(): List<Account> {
        val response = usersService.getAccounts(
            GetAccountsRequest.newBuilder()
                .build(),
        )
        return response.accountsList
            .map(::mapToModelAccount)
    }

    private companion object {

        fun mapToModelAccount(value: DomainAccount): Account =
            Account(
                id = value.id,
                type = mapToModelAccountType(value.type),
                name = value.name,
            )

        fun mapToModelAccountType(value: DomainAccountType): AccountType =
            when (value) {
                DomainAccountType.ACCOUNT_TYPE_TINKOFF -> AccountType.PLAIN
                DomainAccountType.ACCOUNT_TYPE_TINKOFF_IIS -> AccountType.IIS
                DomainAccountType.ACCOUNT_TYPE_INVEST_BOX -> AccountType.INVEST_BOX
                DomainAccountType.ACCOUNT_TYPE_UNSPECIFIED -> error("Account type unspecified")
                DomainAccountType.UNRECOGNIZED -> error("Account type unrecognized")
            }
    }
}
