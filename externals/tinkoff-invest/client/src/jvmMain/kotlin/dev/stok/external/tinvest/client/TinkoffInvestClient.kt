package dev.stok.external.tinvest.client

import dev.stok.external.tinvest.client.grpc.TinkoffInvestAuthInterceptor
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder

actual class TinkoffInvestClient actual constructor(
    token: String,
) {

    private val channel: ManagedChannel = ManagedChannelBuilder.forAddress(GATEWAY_HOST, GATEWAY_PORT)
        .intercept(TinkoffInvestAuthInterceptor(token))
        .build()

    actual val accounts: AccountClient = TinkoffInvestAccountClient(channel)

    actual val instruments: InstrumentClient = TinkoffInvestInstrumentClient(channel)

    actual val orders: OrdersClient = TinkoffInvestOrdersClient(channel)

    private companion object {

        const val GATEWAY_HOST: String = "invest-public-api.tinkoff.ru"

        const val GATEWAY_PORT: Int = 443
    }
}
