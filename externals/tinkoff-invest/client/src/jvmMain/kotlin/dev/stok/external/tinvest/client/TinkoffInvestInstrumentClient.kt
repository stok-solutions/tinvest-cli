package dev.stok.external.tinvest.client

import com.benasher44.uuid.Uuid
import dev.stok.external.tinvest.client.enum.AssetType
import dev.stok.external.tinvest.client.enum.InstrumentType
import dev.stok.external.tinvest.client.enum.TradeClassCode
import dev.stok.external.tinvest.client.model.Asset
import dev.stok.external.tinvest.client.model.Instrument
import io.grpc.ManagedChannel
import ru.tinkoff.piapi.contract.v1.AssetsRequest
import ru.tinkoff.piapi.contract.v1.InstrumentIdType
import ru.tinkoff.piapi.contract.v1.InstrumentRequest
import ru.tinkoff.piapi.contract.v1.InstrumentsServiceGrpcKt.InstrumentsServiceCoroutineStub

private typealias DomainAsset = ru.tinkoff.piapi.contract.v1.Asset
private typealias DomainAssetType = ru.tinkoff.piapi.contract.v1.AssetType

private typealias DomainInstrument = ru.tinkoff.piapi.contract.v1.Instrument
private typealias DomainAssetInstrument = ru.tinkoff.piapi.contract.v1.AssetInstrument
private typealias DomainInstrumentLink = ru.tinkoff.piapi.contract.v1.InstrumentLink
private typealias DomainInstrumentType = ru.tinkoff.piapi.contract.v1.InstrumentType

class TinkoffInvestInstrumentClient(channel: ManagedChannel) : InstrumentClient {

    private val instrumentsService: InstrumentsServiceCoroutineStub = InstrumentsServiceCoroutineStub(channel)

    override suspend fun getAllAssets(): List<Asset> {
        val response = instrumentsService.getAssets(
            AssetsRequest.newBuilder()
                .setInstrumentType(DomainInstrumentType.INSTRUMENT_TYPE_UNSPECIFIED)
                .build(),
        )
        return response.assetsList
            .map(::toModelAsset)
    }

    override suspend fun getAssetsByInstrumentType(type: InstrumentType): List<Asset> {
        val response = instrumentsService.getAssets(
            AssetsRequest.newBuilder()
                .setInstrumentType(toDomainInstrumentType(type))
                .build(),
        )
        return response.assetsList
            .map(::toModelAsset)
    }

    override suspend fun getInstrumentByUid(uid: Uuid): Instrument {
        val response = instrumentsService.getInstrumentBy(
            InstrumentRequest.newBuilder()
                .setIdType(InstrumentIdType.INSTRUMENT_ID_TYPE_UID)
                .setId(uid.toString())
                .build(),
        )
        return toModelInstrument(response.instrument)
    }

    override suspend fun getInstrumentByPositionUid(positionUid: Uuid): Instrument {
        val response = instrumentsService.getInstrumentBy(
            InstrumentRequest.newBuilder()
                .setIdType(InstrumentIdType.INSTRUMENT_ID_TYPE_POSITION_UID)
                .setId(positionUid.toString())
                .build(),
        )
        return toModelInstrument(response.instrument)
    }

    override suspend fun getInstrumentByFigi(figi: String): Instrument {
        val response = instrumentsService.getInstrumentBy(
            InstrumentRequest.newBuilder()
                .setIdType(InstrumentIdType.INSTRUMENT_ID_TYPE_FIGI)
                .setId(figi)
                .build(),
        )
        return toModelInstrument(response.instrument)
    }

    override suspend fun getInstrumentByTicker(ticker: String, classCode: TradeClassCode): Instrument {
        val response = instrumentsService.getInstrumentBy(
            InstrumentRequest.newBuilder()
                .setIdType(InstrumentIdType.INSTRUMENT_ID_TYPE_TICKER)
                .setId(ticker)
                .setClassCode(classCode.code)
                .build(),
        )
        return toModelInstrument(response.instrument)
    }

    private companion object {

        fun toModelAsset(value: DomainAsset): Asset =
            Asset(
                uid = value.uid.let(Uuid::fromString),
                type = toModelAssetType(value.type),
                name = value.name,
                instruments = value.instrumentsList.map(::toModelAssetInstrument),
            )

        fun toModelAssetInstrument(value: DomainAssetInstrument): Asset.AssetInstrument =
            Asset.AssetInstrument(
                uid = value.uid.let(Uuid::fromString),
                type = value.instrumentType,
                kind = value.instrumentKind.let(::toModelInstrumentType),
                figi = value.figi,
                ticker = value.ticker,
                positionUid = value.positionUid.let(Uuid::fromString),
                classCode = value.classCode.let(TradeClassCode::fromCode),
                links = value.linksList.map(::toModelAssetInstrumentLink),
            )

        fun toModelAssetInstrumentLink(value: DomainInstrumentLink): Asset.InstrumentLink =
            Asset.InstrumentLink(
                type = value.type,
                instrumentUid = value.instrumentUid.let(Uuid::fromString),
            )

        fun toModelInstrument(value: DomainInstrument): Instrument =
            Instrument(
                uid = value.uid.let(Uuid::fromString),
                type = value.instrumentType,
                kind = value.instrumentKind.let(::toModelInstrumentType),
                name = value.name,
                figi = value.figi,
                isin = value.isin,
                ticker = value.ticker,
                classCode = value.classCode,
                positionUid = value.positionUid.let(Uuid::fromString),
                exchange = value.exchange,
                currency = value.currency,
                countryOfRisk = Instrument.CountryOfRisk(
                    code = value.countryOfRisk,
                    name = value.countryOfRiskName,
                ),
            )

        fun toModelAssetType(value: DomainAssetType): AssetType =
            when (value) {
                DomainAssetType.ASSET_TYPE_UNSPECIFIED -> AssetType.UNSPECIFIED
                DomainAssetType.ASSET_TYPE_CURRENCY -> AssetType.CURRENCY
                DomainAssetType.ASSET_TYPE_COMMODITY -> AssetType.COMMODITY
                DomainAssetType.ASSET_TYPE_INDEX -> AssetType.INDEX
                DomainAssetType.ASSET_TYPE_SECURITY -> AssetType.SECURITY
                DomainAssetType.UNRECOGNIZED -> AssetType.UNRECOGNIZED
            }

        fun toDomainInstrumentType(value: InstrumentType): DomainInstrumentType =
            when (value) {
                InstrumentType.BOND -> DomainInstrumentType.INSTRUMENT_TYPE_BOND
                InstrumentType.SHARE -> DomainInstrumentType.INSTRUMENT_TYPE_SHARE
                InstrumentType.CURRENCY -> DomainInstrumentType.INSTRUMENT_TYPE_CURRENCY
                InstrumentType.ETF -> DomainInstrumentType.INSTRUMENT_TYPE_ETF
                InstrumentType.FUTURE -> DomainInstrumentType.INSTRUMENT_TYPE_FUTURES
                InstrumentType.STRUCTURAL_NOTE -> DomainInstrumentType.INSTRUMENT_TYPE_SP
                InstrumentType.OPTION -> DomainInstrumentType.INSTRUMENT_TYPE_OPTION
                InstrumentType.CLEARING_CERTIFICATE -> DomainInstrumentType.INSTRUMENT_TYPE_CLEARING_CERTIFICATE
            }

        fun toModelInstrumentType(value: DomainInstrumentType): InstrumentType =
            when (value) {
                DomainInstrumentType.INSTRUMENT_TYPE_BOND -> InstrumentType.BOND
                DomainInstrumentType.INSTRUMENT_TYPE_SHARE -> InstrumentType.SHARE
                DomainInstrumentType.INSTRUMENT_TYPE_CURRENCY -> InstrumentType.CURRENCY
                DomainInstrumentType.INSTRUMENT_TYPE_ETF -> InstrumentType.ETF
                DomainInstrumentType.INSTRUMENT_TYPE_FUTURES -> InstrumentType.FUTURE
                DomainInstrumentType.INSTRUMENT_TYPE_SP -> InstrumentType.STRUCTURAL_NOTE
                DomainInstrumentType.INSTRUMENT_TYPE_OPTION -> InstrumentType.OPTION
                DomainInstrumentType.INSTRUMENT_TYPE_CLEARING_CERTIFICATE -> InstrumentType.CLEARING_CERTIFICATE

                DomainInstrumentType.INSTRUMENT_TYPE_UNSPECIFIED -> TODO()
                DomainInstrumentType.UNRECOGNIZED -> TODO()
            }
    }
}
