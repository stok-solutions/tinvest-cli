package dev.stok.external.tinvest.client

import com.benasher44.uuid.Uuid
import io.grpc.ManagedChannel
import ru.tinkoff.piapi.contract.v1.*
import ru.tinkoff.piapi.contract.v1.OrdersServiceGrpcKt.OrdersServiceCoroutineStub

class TinkoffInvestOrdersClient(channel: ManagedChannel) : OrdersClient {

    private val ordersService: OrdersServiceCoroutineStub = OrdersServiceCoroutineStub(channel)

    override suspend fun marketBuy(instrumentId: String, quantity: Long, accountId: String): Uuid {
        val orderId = Uuid.randomUUID()
        ordersService.postOrder(
            PostOrderRequest.newBuilder()
                .setOrderType(OrderType.ORDER_TYPE_MARKET)
                .setDirection(OrderDirection.ORDER_DIRECTION_BUY)
                .setInstrumentId(instrumentId)
                .setQuantity(quantity)
                .setAccountId(accountId)
                .setOrderId(orderId.toString())
                .build(),
        )
        return orderId
    }

    override suspend fun marketSell(instrumentId: String, quantity: Long, accountId: String): Uuid {
        val orderId = Uuid.randomUUID()
        ordersService.postOrder(
            PostOrderRequest.newBuilder()
                .setOrderType(OrderType.ORDER_TYPE_MARKET)
                .setDirection(OrderDirection.ORDER_DIRECTION_SELL)
                .setInstrumentId(instrumentId)
                .setQuantity(quantity)
                .setAccountId(accountId)
                .setOrderId(orderId.toString())
                .build(),
        )
        return orderId
    }

    override suspend fun limitBuy(instrumentId: String, price: Double, quantity: Long, accountId: String): Uuid {
        val orderId = Uuid.randomUUID()
        ordersService.postOrder(
            PostOrderRequest.newBuilder()
                .setOrderType(OrderType.ORDER_TYPE_LIMIT)
                .setDirection(OrderDirection.ORDER_DIRECTION_BUY)
                .setInstrumentId(instrumentId)
                .setPrice(price.toQuotation())
                .setQuantity(quantity)
                .setAccountId(accountId)
                .setOrderId(orderId.toString())
                .build(),
        )
        return orderId
    }

    override suspend fun limitSell(instrumentId: String, price: Double, quantity: Long, accountId: String): Uuid {
        val orderId = Uuid.randomUUID()
        ordersService.postOrder(
            PostOrderRequest.newBuilder()
                .setOrderType(OrderType.ORDER_TYPE_LIMIT)
                .setDirection(OrderDirection.ORDER_DIRECTION_SELL)
                .setInstrumentId(instrumentId)
                .setPrice(price.toQuotation())
                .setQuantity(quantity)
                .setAccountId(accountId)
                .setOrderId(orderId.toString())
                .build(),
        )
        return orderId
    }

    override suspend fun cancelOrder(orderId: Uuid, accountId: String) {
        val response = ordersService.cancelOrder(
            CancelOrderRequest.newBuilder()
                .setAccountId(accountId)
                .setOrderId(orderId.toString())
                .build(),
        )
    }

    private companion object {

        private fun Double.toQuotation(): Quotation {
            val units = this.toLong()
            val nano = ((this - units) * 1_000_000_000).toInt()

            return Quotation.newBuilder()
                .setUnits(units)
                .setNano(nano)
                .build()
        }
    }
}
