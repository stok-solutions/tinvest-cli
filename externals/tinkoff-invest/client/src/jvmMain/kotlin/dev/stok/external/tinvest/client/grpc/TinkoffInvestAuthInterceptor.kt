package dev.stok.external.tinvest.client.grpc

import io.grpc.CallCredentials
import io.grpc.CallOptions
import io.grpc.Channel
import io.grpc.ClientCall
import io.grpc.ClientInterceptor
import io.grpc.Metadata
import io.grpc.MethodDescriptor
import java.util.concurrent.Executor

internal class TinkoffInvestAuthInterceptor(private val token: String) : ClientInterceptor {

    override fun <ReqT, RespT> interceptCall(
        method: MethodDescriptor<ReqT, RespT>,
        callOptions: CallOptions,
        next: Channel,
    ): ClientCall<ReqT, RespT> =
        next.newCall(
            method,
            callOptions.withCallCredentials(TinkoffInvestApiCallCredentials(token)),
        )

    private class TinkoffInvestApiCallCredentials(private val token: String) : CallCredentials() {

        override fun applyRequestMetadata(
            requestInfo: RequestInfo,
            appExecutor: Executor,
            applier: MetadataApplier,
        ) {
            val headers = Metadata().apply {
                put(AUTHORIZATION_METADATA_KEY, "Bearer $token")
            }
            applier.apply(headers)
        }

        companion object {

            private val AUTHORIZATION_METADATA_KEY: Metadata.Key<String> =
                Metadata.Key.of("Authorization", Metadata.ASCII_STRING_MARSHALLER)
        }
    }
}
