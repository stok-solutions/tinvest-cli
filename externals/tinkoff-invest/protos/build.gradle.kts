plugins {
    id(BuildPlugins.TINVESTCLI_BASE)
    `java-library`
}

java {
    sourceSets.getByName("main").resources.srcDir("src/main/proto")
}
